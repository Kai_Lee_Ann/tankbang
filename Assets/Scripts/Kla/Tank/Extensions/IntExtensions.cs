﻿namespace Kla.Tank
{
    public static class IntExtensions
    {
        private const int MillisecondsMultiplier = 1000;
        public static float ToSeconds(this int milliseconds) => (float) milliseconds / MillisecondsMultiplier;
    }
}
