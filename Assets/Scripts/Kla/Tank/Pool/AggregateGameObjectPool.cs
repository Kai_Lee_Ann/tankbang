﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Kla.Tank
{
    /// <summary>
    /// Коллекция пулов для хранения наборов GameObject и обращения к ним по ключу
    /// </summary>
    public class AggregateGameObjectPool<TKey, TComponent> where TComponent : Component
    {
        private readonly Dictionary<TKey, GameObjectPool<TComponent>> _poolsByKey;

        public AggregateGameObjectPool(int initPoolCount = 16) =>
            _poolsByKey = new Dictionary<TKey, GameObjectPool<TComponent>>(initPoolCount);

        public void Add(TKey key, Func<TComponent> factoryFunc, int initPoolSize = 16) =>
            _poolsByKey[key] = new GameObjectPool<TComponent>(factoryFunc, initPoolSize);

        public TComponent Get(TKey key) => _poolsByKey[key].Get();

        public void Reclaim(TKey key, TComponent value) => _poolsByKey[key].Reclaim(value);
    }
}
