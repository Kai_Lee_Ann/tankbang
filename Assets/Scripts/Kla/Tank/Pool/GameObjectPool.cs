﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Kla.Tank
{
    /// <summary>
    /// Простой пул для GameObject, с обращением к ним по одному компоненту. Отключает gameObject при возвращении и
    /// включает при выдаче.
    /// </summary>
    public class GameObjectPool<TComponent> where TComponent : Component
    {
        private readonly Func<TComponent> _factoryFunction;
        private readonly Stack<TComponent> _objects;

        public GameObjectPool(Func<TComponent> factoryFunction, int initSize = 16)
        {
            _objects = new Stack<TComponent>(initSize);
            _factoryFunction = factoryFunction;

            for (var i = 0; i < initSize; i++)
            {
                var component = Create();
                component.gameObject.SetActive(false);
            }
        }

        public TComponent Get() => _objects.Count > 0 ? GetLastElement() : Create();

        public void Reclaim(TComponent component)
        {
            component.gameObject.SetActive(false);
            _objects.Push(component);
        }

        private TComponent Create()
        {
            var component = _factoryFunction.Invoke();
            _objects.Push(component);

            return component;
        }

        private TComponent GetLastElement()
        {
            var component = _objects.Pop();
            component.gameObject.SetActive(true);

            return component;
        }
    }
}
