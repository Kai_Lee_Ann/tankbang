﻿using UnityEngine;

namespace Kla.Tank
{
    [CreateAssetMenu(fileName = "EnemySettings", menuName = "KLA/Tank/PlayerSettings")]
    public class PlayerSettings : ScriptableObject
    {
        public HealthSettings HealthSettings;
        public MovementSettings MovementSettings;
    }
}
