﻿using UnityEngine;

namespace Kla.Tank
{
    [CreateAssetMenu(fileName = "CannonSettings", menuName = "KLA/Tank/CannonSettings")]
    public class CannonSettings : ScriptableObject
    {
        [Header("Settings")]
        public DamageSettings DamageSettings;
        [Tooltip("Время перезарядки между выстрелами в миллисекундах")]
        [Min(0)] public int CooldownTimeMilliseconds;
        [Min(0)] public float ProjectileMovementSpeed;
        [Tooltip("Через сколько секунд выстрел автоматически уничтожится")]
        [Min(0)] public int TimeToLiveMilliseconds = 3000;
        [Tooltip("Сколько снарядов держать в пуле объектов")]
        [Min(0)] public int InitPoolSize = 10;

        [Header("Prefabs")]
        [Tooltip("Префаб пушки")]
        public CannonView CannonViewPrefab;
        [Tooltip("Префаб выстрела")]
        public ProjectileFacade ProjectileFacadePrefab;
    }
}
