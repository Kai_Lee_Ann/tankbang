﻿using UnityEngine;

namespace Kla.Tank
{
    [CreateAssetMenu(fileName = "EnemySettings", menuName = "KLA/Tank/EnemySettings")]
    public class EnemySettings : ScriptableObject
    {
        [Header("Settings")]
        public DamageSettings DamageSettings;
        public HealthSettings HealthSettings;
        public MovementSettings MovementSettings;

        [Header("Prefabs")]
        public EnemyFacade EnemyFacadePrefab;
    }
}
