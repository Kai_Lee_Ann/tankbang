﻿using UnityEngine;

namespace Kla.Tank
{
    [CreateAssetMenu(fileName = "UISettings", menuName = "KLA/Tank/UISettings")]
    public class UISettings : ScriptableObject
    {
        public Color playerHealthBar;
        public Color enemyHealthBar;
    }
}
