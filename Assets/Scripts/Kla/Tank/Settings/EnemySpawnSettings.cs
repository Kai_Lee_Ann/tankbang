﻿using UnityEngine;

namespace Kla.Tank
{
    [CreateAssetMenu(fileName = "EnemySpawnSettings", menuName = "KLA/Tank/EnemySpawnSettings")]
    public class EnemySpawnSettings : ScriptableObject
    {
        [Tooltip("Максимальное число противников на уровне")]
        [Min(0)] public int maxEnemyCount = 10;
        [Tooltip("На каком расстоянии от центра создавать новых монстров")]
        public float spawnDistance = 10f;
    }
}
