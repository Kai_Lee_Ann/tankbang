﻿using System;

namespace Kla.Tank
{
    [Serializable]
    public class DamageSettings
    {
        public float damage;
        public DamageLayer damageLayer;
    }
}
