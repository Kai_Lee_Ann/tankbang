﻿using System;
using UnityEngine;

namespace Kla.Tank
{
    [Serializable]
    public class MovementSettings
    {
        [Tooltip("Скорость в метрах в секунду")]
        [Range(0, 25f)] public float MoveSpeed = 4f;
        [Tooltip("Скорость поворота в градусах в секунду")]
        [Range(0, 360f)] public float RotationSpeed = 150f;
    }
}
