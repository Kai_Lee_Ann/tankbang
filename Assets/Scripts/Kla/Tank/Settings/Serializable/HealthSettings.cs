﻿using System;
using UnityEngine;

namespace Kla.Tank
{
    [Serializable]
    public class HealthSettings
    {
        [Min(0)] public float MaxHealth;
        [Range(0, 1)] public float Defense;
        [Tooltip("Сколько времени после получения урона игнроирвать новые повреждения")]
        [Min(0)] public float InvulnerabilityTime;
        public DamageLayer DamageLayer;
    }
}
