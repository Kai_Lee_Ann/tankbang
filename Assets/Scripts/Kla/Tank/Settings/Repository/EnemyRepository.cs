﻿using UnityEngine;

namespace Kla.Tank
{
    [CreateAssetMenu(fileName = "EnemyRepository", menuName = "KLA/Tank/Repository/EnemyRepository")]
    public class EnemyRepository : ScriptableObject
    {
        public EnemySettings[] Enemies;
    }
}
