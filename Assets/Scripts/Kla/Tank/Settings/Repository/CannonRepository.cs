﻿using UnityEngine;

namespace Kla.Tank
{
    [CreateAssetMenu(fileName = "CannonRepository", menuName = "KLA/Tank/Repository/CannonRepository")]
    public class CannonRepository : ScriptableObject
    {
        public CannonSettings[] Cannons;
    }
}
