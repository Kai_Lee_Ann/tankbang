﻿using UnityEngine;

namespace Kla.Tank
{
    public class EnemyMover : MonoBehaviour
    {
        public Transform SearchTarget { get; set; }
        public float MoveSpeed => _movementSettings.MoveSpeed;
        private float RotationSpeed => _movementSettings.RotationSpeed;

        private MovementSettings _movementSettings;

        private void Update()
        {
            if (IsTargetReached())
                return;

            RotateTowards(SearchTarget.position);
            MoveForward();
        }


        public void SetSpeed(MovementSettings movementSettings) => _movementSettings = movementSettings;

        private bool IsTargetReached()
        {
            const float reachDistance = 0.001f;
            return (SearchTarget.position - transform.position).sqrMagnitude < reachDistance;
        }

        private void MoveForward()
        {
            float magnitude = MoveSpeed * Time.deltaTime;
            transform.Translate(Vector3.forward * magnitude);
        }

        private void RotateTowards(Vector3 position)
        {
            var transformCache = transform;

            Vector3 targetDirection = position - transformCache.position;
            float maxRadiansDelta = RotationSpeed * Time.deltaTime;
            Vector3 newDirection = Vector3.RotateTowards(transformCache.forward, targetDirection, maxRadiansDelta,
                0.0f);
            transformCache.rotation = Quaternion.LookRotation(newDirection);
        }
    }
}
