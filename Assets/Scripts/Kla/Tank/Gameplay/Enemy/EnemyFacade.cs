﻿using System;
using UnityEngine;

namespace Kla.Tank
{
    public class EnemyFacade : MonoBehaviour, IUnit
    {
        [SerializeField] private ContactDamage contactDamage;
        [SerializeField] private Health health;
        [SerializeField] private EnemyMover mover;

        public Vector3 Position => transform.position;
        public float MoveSpeed => mover.MoveSpeed;
        public float Damage => contactDamage.Damage;
        public Health Health => health;
        public EnemySettings Settings { get; private set; }

        public event Action<EnemyFacade> OnHealthDeplete;

        private void OnEnable() => health.OnDeplete += OnDepleted;

        private void OnDisable() => health.OnDeplete -= OnDepleted;


        public EnemyFacade Init(EnemySettings enemySettings)
        {
            Settings = enemySettings;

            SetDamage(enemySettings.DamageSettings)
                .SetHealth(enemySettings.HealthSettings)
                .SetMovementSettings(enemySettings.MovementSettings);

            return this;
        }

        public EnemyFacade SetSearchTarget(Transform searchTarget)
        {
            if (mover)
                mover.SearchTarget = searchTarget;

            return this;
        }

        public void ResetState()
        {
            if (health)
                health.RestoreFull();
        }

        private EnemyFacade SetDamage(DamageSettings damageSettings)
        {
            if (contactDamage)
                contactDamage.SetDamage(damageSettings);

            return this;
        }

        private EnemyFacade SetHealth(HealthSettings healthSettings)
        {
            if (health)
                health.Init(healthSettings);

            return this;
        }

        private EnemyFacade SetMovementSettings(MovementSettings speed)
        {
            if (mover)
                mover.SetSpeed(speed);

            return this;
        }

        private void OnDepleted(Health _) => OnHealthDeplete?.Invoke(this);
    }
}
