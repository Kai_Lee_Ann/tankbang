﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;


namespace Kla.Tank
{
    /// <summary>
    /// Контроллирует жизненный цикл противников и их число на сцене
    /// </summary>
    public class EnemySpawner : MonoBehaviour
    {
        [SerializeField] private Transform target;
        [SerializeField] private EnemyRepository enemyRepository;
        [SerializeField] private EnemySpawnSettings EnemySpawnSettings;
        [SerializeField] private Transform spawnPoint;

        public event Action<EnemyFacade> OnEnemySpawn;
        public event Action<EnemyFacade> OnEnemyDestroy;


        private AggregateGameObjectPool<EnemySettings, EnemyFacade> _enemyPool =
            new AggregateGameObjectPool<EnemySettings, EnemyFacade>();

        private int _enemyCount;

        private void Awake()
        {
            EnemySettings[] enemies = enemyRepository.Enemies;
            _enemyPool = new AggregateGameObjectPool<EnemySettings, EnemyFacade>(enemies.Length);

            int maxEnemyCount = EnemySpawnSettings.maxEnemyCount;
            foreach (EnemySettings enemy in enemies)
                InitPool(enemy, maxEnemyCount);
        }

        private void Start()
        {
            for (int i = 0; i < EnemySpawnSettings.maxEnemyCount; i++)
                SpawnRandomEnemy();
        }


        private void InitPool(EnemySettings enemy, int maxEnemyCount)
        {
            GameObject poolContainer = new GameObject($"{enemy.name}Pool");
            EnemyFacade Create() => CreateEnemy(enemy, poolContainer.transform);
            _enemyPool.Add(enemy, Create, maxEnemyCount);
        }

        private EnemyFacade CreateEnemy(EnemySettings enemySettings, Transform parent)
        {
            var enemy = Instantiate(enemySettings.EnemyFacadePrefab)
                .Init(enemySettings)
                .SetSearchTarget(target);

            enemy.transform.parent = parent;

            return enemy;
        }

        private Vector3 GetRandomPosition()
        {
            Vector2 insideUnitCircle = Random.insideUnitCircle.normalized;
            Vector3 direction = new Vector3(insideUnitCircle.x, 0, insideUnitCircle.y);

            return spawnPoint.position + direction * EnemySpawnSettings.spawnDistance;
        }

        private void SpawnRandomEnemy()
        {
            if (_enemyCount <= EnemySpawnSettings.maxEnemyCount)
                SpawnEnemy(GetRandomEnemy());
        }

        private EnemySettings GetRandomEnemy()
        {
            var enemies = enemyRepository.Enemies;
            var index = Random.Range(0, enemies.Length);
            var enemySettings = enemies[index];

            return enemySettings;
        }

        private void SpawnEnemy(EnemySettings enemySettings)
        {
            EnemyFacade enemy = _enemyPool.Get(enemySettings);
            enemy.transform.position = GetRandomPosition();
            enemy.OnHealthDeplete += OnHealthDepleted;
            _enemyCount++;

            OnEnemySpawn?.Invoke(enemy);
            enemy.ResetState();
        }

        private void OnHealthDepleted(EnemyFacade enemy)
        {
            enemy.OnHealthDeplete -= OnHealthDepleted;
            _enemyPool.Reclaim(enemy.Settings, enemy);
            _enemyCount--;

            OnEnemyDestroy?.Invoke(enemy);
            SpawnRandomEnemy();
        }
    }
}
