﻿using UnityEngine;

namespace Kla.Tank
{
    public class CameraFollower : MonoBehaviour
    {
        [SerializeField] private Transform camTransform;
        [SerializeField] private Transform target;
        [SerializeField] private float smoothTime = 0.3f;

        private Vector3 _velocity;
        private Vector3 _offset;

        private void Start() => _offset = camTransform.position - target.position;

        private void LateUpdate()
        {
            Vector3 targetPosition = target.position + _offset;
            camTransform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref _velocity, smoothTime);
        }
    }
}
