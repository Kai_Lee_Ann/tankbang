﻿namespace Kla.Tank
{
    /// <summary>
    /// Используется, чтобы разделять урон по игроку и по противникам
    /// </summary>
    [System.Flags]
    public enum DamageLayer
    {
        None,
        Player,
        Enemy
    }
}
