﻿using System;
using UnityEngine;

namespace Kla.Tank
{
    public class ContactDamage : MonoBehaviour
    {
        public float Damage => _damageSettings.damage;
        private DamageLayer DamageLayer => _damageSettings.damageLayer;
        public event Action OnHit;


        private DamageSettings _damageSettings;

        private void OnTriggerStay(Collider other)
        {
            if (!other.gameObject.TryGetComponent<Health>(out var health))
                return;

            if (!DamageLayer.HasFlag(health.DamageLayer))
                return;

            health.TakeDamage(Damage);
            OnHit?.Invoke();
        }

        public void SetDamage(DamageSettings damageSettings)
        {
            _damageSettings = damageSettings;
        }
    }
}
