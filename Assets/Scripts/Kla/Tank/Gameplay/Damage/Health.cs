﻿using System;
using UnityEngine;

namespace Kla.Tank
{
    public class Health : MonoBehaviour
    {
        [SerializeField] private Invulnerability _invul;

        public float CurrentHealth { get; private set; }
        public float MaxHealth => _healthSettings.MaxHealth;
        public float Defense => _healthSettings.Defense;
        public DamageLayer DamageLayer => _healthSettings.DamageLayer;

        public event Action OnChange;
        public event Action<Health> OnDeplete;


        private HealthSettings _healthSettings;

        public void Init(HealthSettings healthSettings)
        {
            _healthSettings = healthSettings;
            UpdateHealth(_healthSettings.MaxHealth);
        }

        public void RestoreFull() => UpdateHealth(_healthSettings.MaxHealth);

        public void TakeDamage(float damage)
        {
            // Если неуязвимость активна, игнорируем получение урона
            if (_invul.IsActive)
                return;

            // Если здоровье ниже нуля, игнорируем получение урона
            if (CurrentHealth <= 0f)
                return;

            var totalDamage = CalculateDamage(damage);
            UpdateHealth(CurrentHealth - totalDamage);
            Debug.Log($"Damage taken by {gameObject.name}: {totalDamage}");

            if (CurrentHealth <= 0f)
            {
                // Не даем опустить значение меньше 0
                UpdateHealth(0f);
                OnDeplete?.Invoke(this);
                return;
            }

            // Включаем неуязвимость после получения урона
            _invul.Activate(_healthSettings.InvulnerabilityTime);
        }

        private void UpdateHealth(float value)
        {
            CurrentHealth = value;
            OnChange?.Invoke();
        }

        private float CalculateDamage(float damage) => damage * (1f - Mathf.Clamp01(Defense));
    }
}
