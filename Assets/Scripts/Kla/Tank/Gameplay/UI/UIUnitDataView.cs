﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Kla.Tank
{
    public class UIUnitDataView : MonoBehaviour
    {
        [Header("Components")]
        [SerializeField] private TMP_Text defenseValue;
        [SerializeField] private TMP_Text hpValue;
        [SerializeField] private TMP_Text moveSpeedValue;
        [SerializeField] private TMP_Text damageValue;
        [SerializeField] private Slider healthSlider;
        [SerializeField] private Image healthFillArea;

        [Header("Settings")]
        [SerializeField] private Vector3 offset;

        private IUnit _target;
        private Camera _camera;

        private void Update()
        {
            UpdatePosition();
            UpdateParams();
        }


        public void Init(IUnit target, Camera camera, Color color = default)
        {
            _camera = camera;
            _target = target;

            _target.Health.OnChange += OnHealthChanged;
            _target.Health.OnDeplete += OnHealthDepleted;

            if (color != default)
                UpdateHealthBarColor(color);

            UpdatePosition();
        }

        private void UpdateHealthBarColor(Color color)
        {
            var newColor = color;
            newColor.a = healthFillArea.color.a;
            healthFillArea.color = newColor;
        }

        private void OnHealthDepleted(Health health)
        {
            health.OnChange -= OnHealthChanged;
            health.OnDeplete -= OnHealthDepleted;
        }

        private void OnHealthChanged() => UpdateHealth();

        private void UpdatePosition() =>
            transform.position = _camera.WorldToScreenPoint(_target.Position) + offset;

        private void UpdateParams()
        {
            UpdateText(damageValue, $"Damage: {_target.Damage}");
            UpdateText(defenseValue, $"Defense: {_target.Health.Defense:N2}");
            UpdateText(moveSpeedValue, $"Move Speed: {_target.MoveSpeed}");
        }

        private void UpdateHealth()
        {
            Health health = _target.Health;
            healthSlider.value = health.CurrentHealth / health.MaxHealth;
            UpdateText(hpValue, $"Health: {health.CurrentHealth:N0}/{health.MaxHealth:N0}");
        }

        private void UpdateText(TMP_Text text, string value)
        {
            if (text.text != value)
                text.text = value;
        }
    }
}
