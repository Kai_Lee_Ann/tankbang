﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Kla.Tank
{
    public class UIGameOverView : MonoBehaviour
    {
        [SerializeField] private Transform _container;
        [SerializeField] private Button _restartButton;
        [SerializeField] private LoseConditionChecker _loseConditionChecker;

        private void Awake() => ShowView(false);

        private void OnEnable()
        {
            _loseConditionChecker.OnPlayerLost += OnPlayerLost;
            _restartButton.onClick.AddListener(OnRestartButtonClick);
        }

        private void OnDisable()
        {
            _loseConditionChecker.OnPlayerLost -= OnPlayerLost;
            _restartButton.onClick.RemoveListener(OnRestartButtonClick);
        }


        private void OnPlayerLost() => ShowView(true);

        private void ShowView(bool show) => _container.gameObject.SetActive(show);

        private void OnRestartButtonClick() => SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
