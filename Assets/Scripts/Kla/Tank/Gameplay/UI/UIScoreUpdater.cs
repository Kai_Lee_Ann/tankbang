﻿using System;
using TMPro;
using UnityEngine;

namespace Kla.Tank
{
    public class UIScoreUpdater : MonoBehaviour
    {
        private const string ScorePrefix = "Score {0}";

        [SerializeField] private EnemySpawner enemySpawner;
        [SerializeField] private TMP_Text scoreText;

        private long _currentScore;
        private bool _isScoreOverflew;

        private void OnEnable() => enemySpawner.OnEnemyDestroy += OnEnemyDestroyed;

        private void OnDisable() => enemySpawner.OnEnemyDestroy -= OnEnemyDestroyed;

        private void Start() => UpdateScore();


        private void OnEnemyDestroyed(EnemyFacade enemyFacade)
        {
            IncrementScore(enemyFacade.Health);
            UpdateScore();
        }

        private void IncrementScore(Health health)
        {
            if (_isScoreOverflew)
                return;

            checked
            {
                try
                {
                    _currentScore += (int) (health.MaxHealth * (1 + health.Defense));
                }
                catch (OverflowException)
                {
                    SetInfiniteScore();
                }
            }
        }

        private void SetInfiniteScore()
        {
            _isScoreOverflew = true;
            scoreText.text = string.Format(ScorePrefix, "Infinity");
        }

        private void UpdateScore()
        {
            if (_isScoreOverflew)
                return;

            scoreText.text = string.Format(ScorePrefix, _currentScore);
        }
    }
}
