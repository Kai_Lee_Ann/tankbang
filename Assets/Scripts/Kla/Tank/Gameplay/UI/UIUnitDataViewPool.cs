﻿using System.Collections.Generic;
using UnityEngine;

namespace Kla.Tank
{
    public class UIUnitDataViewPool : MonoBehaviour
    {
        [Header("Components")]
        [SerializeField] private Camera mainCamera;

        [Header("Settings")]
        [SerializeField] private UISettings uiSettings;
        [SerializeField] private int initPoolSize = 11;

        [Header("Prefabs")]
        [SerializeField] private UIUnitDataView prefab;

        [Header("Dependencies")]
        [SerializeField] private EnemySpawner enemySpawner;
        [SerializeField] private TankFacade tank;

        private GameObjectPool<UIUnitDataView> _pool;
        private Dictionary<IUnit, UIUnitDataView> _viewsByUnit;

        private void Awake()
        {
            const string poolName = "UIUnitDataViewPool";

            GameObject poolObject = new GameObject(poolName);
            poolObject.transform.parent = transform;

            UIUnitDataView Create() => Instantiate(prefab, poolObject.transform);
            _pool = new GameObjectPool<UIUnitDataView>(Create, initPoolSize);

            _viewsByUnit = new Dictionary<IUnit, UIUnitDataView>();
        }

        private void OnEnable()
        {
            Add(tank, uiSettings.playerHealthBar);

            enemySpawner.OnEnemySpawn += OnEnemySpawned;
            enemySpawner.OnEnemyDestroy += OnEnemyDestroyed;
        }

        private void OnDisable()
        {
            Remove(tank);

            enemySpawner.OnEnemySpawn -= OnEnemySpawned;
            enemySpawner.OnEnemyDestroy -= OnEnemyDestroyed;
        }


        private void OnEnemySpawned(EnemyFacade enemy) => Add(enemy, uiSettings.enemyHealthBar);

        private void OnEnemyDestroyed(EnemyFacade enemy) => Remove(enemy);

        private void Add(IUnit target, Color color = default)
        {
            var view = _pool.Get();
            view.Init(target, mainCamera, color);
            _viewsByUnit[target] = view;
        }

        private void Remove(IUnit target)
        {
            var view = _viewsByUnit[target];
            _pool.Reclaim(view);
        }
    }
}
