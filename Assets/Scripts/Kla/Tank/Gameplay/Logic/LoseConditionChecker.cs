﻿using System;
using UnityEngine;

namespace Kla.Tank
{
    public class LoseConditionChecker : MonoBehaviour
    {
        [SerializeField] private Health tankHealth;

        public event Action OnPlayerLost;

        public void OnEnable() => tankHealth.OnDeplete += OnDepleted;

        private void OnDisable() => tankHealth.OnDeplete -= OnDepleted;


        private void OnDepleted(Health obj)
        {
            OnPlayerLost?.Invoke();
            Debug.Log("Player lost");
        }
    }
}
