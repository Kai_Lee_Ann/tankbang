﻿using UnityEngine;

namespace Kla.Tank
{
    public interface IUnit
    {
        Vector3 Position { get; }
        float MoveSpeed { get; }
        float Damage { get; }
        Health Health { get; }
    }
}
