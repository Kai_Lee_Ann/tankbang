﻿using UnityEngine;

namespace Kla.Tank
{
    public class ProjectileMover : MonoBehaviour
    {
        private float _speed;

        private void Update() => MoveForward();


        public void SetMovementSpeed(float speed) => _speed = speed;

        private void MoveForward() => transform.Translate(Vector3.forward * (_speed * Time.deltaTime));
    }
}
