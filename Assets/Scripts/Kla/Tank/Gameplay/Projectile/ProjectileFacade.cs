﻿using System;
using System.Collections;
using UnityEngine;

namespace Kla.Tank
{
    public class ProjectileFacade : MonoBehaviour
    {
        [SerializeField] private ProjectileMover mover;
        [SerializeField] private ContactDamage contactDamage;

        public CannonSettings CannonSettings { get; private set; }

        public event Action<ProjectileFacade> OnDestroy;

        private Coroutine timeToLiveCoroutine;

        private void OnEnable()
        {
            if (contactDamage)
                contactDamage.OnHit += OnContactHit;
        }

        private void OnDisable()
        {
            if (contactDamage)
                contactDamage.OnHit -= OnContactHit;
        }


        public ProjectileFacade Init(CannonSettings cannonSettings)
        {
            CannonSettings = cannonSettings;

            SetDamage(cannonSettings.DamageSettings)
                .SetMovementSpeed(cannonSettings.ProjectileMovementSpeed);

            float secondsToLive = CannonSettings.TimeToLiveMilliseconds.ToSeconds();
            timeToLiveCoroutine = StartCoroutine(TimeToLiveCoroutine(secondsToLive));

            return this;
        }

        public ProjectileFacade SetPosition(Vector3 position)
        {
            transform.position = position;
            return this;
        }

        public ProjectileFacade SetDirection(Vector3 direction)
        {
            transform.forward = direction;
            return this;
        }

        private IEnumerator TimeToLiveCoroutine(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            Destroy();
        }

        private ProjectileFacade SetMovementSpeed(float speed)
        {
            if (mover)
                mover.SetMovementSpeed(speed);

            return this;
        }

        private ProjectileFacade SetDamage(DamageSettings damageSettings)
        {
            if (contactDamage)
                contactDamage.SetDamage(damageSettings);

            return this;
        }

        private void OnContactHit() => Destroy();

        private void Destroy()
        {
            if (timeToLiveCoroutine != null)
                StopCoroutine(timeToLiveCoroutine);

            OnDestroy?.Invoke(this);
        }
    }
}
