﻿using System.Collections.Generic;
using UnityEngine;

namespace Kla.Tank
{
    public class CannonPool : MonoBehaviour
    {
        [SerializeField] private CannonRepository cannonRepository;

        private readonly Dictionary<CannonSettings, CannonView> _pool = new Dictionary<CannonSettings, CannonView>();

        private void Awake()
        {
            foreach (var cannon in cannonRepository.Cannons)
                Create(cannon);
        }

        public CannonView Get(CannonSettings cannonSettings)
        {
            var view = _pool[cannonSettings];
            view.gameObject.SetActive(true);
            return view;
        }

        public void Reclaim(CannonSettings cannonSettings) =>
            _pool[cannonSettings].gameObject.SetActive(false);

        private void Create(CannonSettings cannon)
        {
            var cannonView = Instantiate(cannon.CannonViewPrefab, transform);
            cannonView.gameObject.SetActive(false);
            _pool[cannon] = cannonView;
        }
    }
}
