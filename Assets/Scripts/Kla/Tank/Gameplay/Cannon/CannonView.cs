﻿using UnityEngine;

namespace Kla.Tank
{
    public class CannonView : MonoBehaviour
    {
        [SerializeField] private Transform projectileSpawnPoint;

        public Vector3 GetSpawnPoint() => projectileSpawnPoint.position;

        public Vector3 GetSpawnDirection() => transform.forward;
    }
}
