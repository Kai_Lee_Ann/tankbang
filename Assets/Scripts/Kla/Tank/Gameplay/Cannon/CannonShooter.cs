﻿using UnityEngine;

namespace Kla.Tank
{
    public class CannonShooter : MonoBehaviour
    {
        [SerializeField] private TankView tankView;
        [SerializeField] private GameInput gameInput;
        [SerializeField] private CannonSwitcher cannonSwitcher;
        [SerializeField] private Cooldown cooldown;
        [SerializeField] private CannonRepository cannonRepository;

        public float Damage => CurrentCannon.DamageSettings.damage;
        private CannonSettings CurrentCannon => cannonSwitcher.CurrentCannon;


        private readonly AggregateGameObjectPool<CannonSettings, ProjectileFacade> _projectilePool =
            new AggregateGameObjectPool<CannonSettings, ProjectileFacade>();


        private void Awake()
        {
            var cannons = cannonRepository.Cannons;
            foreach (CannonSettings cannon in cannons)
                InitPool(cannon);
        }

        private void OnEnable()
        {
            gameInput.OnShootPress += OnShootPressed;
            cannonSwitcher.OnCannonSwitch += OnCannonSwitched;
        }

        private void OnDisable()
        {
            gameInput.OnShootPress -= OnShootPressed;
            cannonSwitcher.OnCannonSwitch -= OnCannonSwitched;
        }


        private void InitPool(CannonSettings cannon)
        {
            var parent = new GameObject($"{cannon.name}Pool");
            _projectilePool.Add(cannon, () => Create(cannon, parent.transform), cannon.InitPoolSize);
        }

        private ProjectileFacade Create(CannonSettings cannon, Transform parent) =>
            Instantiate(cannon.ProjectileFacadePrefab, parent);

        private void OnCannonSwitched(CannonSettings _) => cooldown.StopCooldown();

        private void OnShootPressed()
        {
            if (cooldown.IsActive)
                return;

            SpawnProjectile();

            cooldown.StartCooldown(CurrentCannon.CooldownTimeMilliseconds);
        }

        private void SpawnProjectile()
        {
            var projectile = _projectilePool.Get(CurrentCannon);
            projectile.Init(CurrentCannon);
            projectile.OnDestroy += OnProjectileDestroyed;

            var cannonView = tankView.CannonView;

            projectile.SetPosition(cannonView.GetSpawnPoint())
                .SetDirection(cannonView.GetSpawnDirection());
        }

        private void OnProjectileDestroyed(ProjectileFacade projectile) => Remove(projectile);

        private void Remove(ProjectileFacade projectile)
        {
            projectile.OnDestroy -= OnProjectileDestroyed;
            _projectilePool.Reclaim(projectile.CannonSettings, projectile);
        }
    }
}
