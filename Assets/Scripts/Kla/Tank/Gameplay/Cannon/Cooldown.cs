﻿using System.Collections;
using UnityEngine;

namespace Kla.Tank
{
    public class Cooldown : MonoBehaviour
    {
        public bool IsActive { get; private set; }

        private Coroutine _cooldownCoroutine;
        private float _cooldownTimeLeft;

        public void StartCooldown(int cooldownTimeMilliseconds)
        {
            StopCooldown();

            IsActive = true;
            _cooldownCoroutine = StartCoroutine(CooldownCoroutine(cooldownTimeMilliseconds));
        }

        public void StopCooldown()
        {
            if (_cooldownCoroutine != null)
                StopCoroutine(_cooldownCoroutine);

            IsActive = false;
        }

        private IEnumerator CooldownCoroutine(int cooldownTimeMilliseconds)
        {
            _cooldownTimeLeft = 0.0f;

            var timeMilliseconds = cooldownTimeMilliseconds.ToSeconds();
            while (_cooldownTimeLeft < timeMilliseconds)
            {
                _cooldownTimeLeft += Time.deltaTime;
                yield return null;
            }

            IsActive = false;
        }
    }
}
