﻿using System;
using UnityEngine;

namespace Kla.Tank
{
    public class CannonSwitcher : MonoBehaviour
    {
        [SerializeField] private CannonRepository cannonRepository;
        [SerializeField] private GameInput gameInput;

        public CannonSettings CurrentCannon => cannonRepository.Cannons[_canonIndex];
        private CannonSettings[] Cannons => cannonRepository.Cannons;

        public event Action<CannonSettings> OnCannonSwitch;

        private int _canonIndex;

        private void Start() => OnCannonSwitch?.Invoke(CurrentCannon);

        private void OnEnable()
        {
            gameInput.OnNextCannonPress += SwitchToNextCannon;
            gameInput.OnPreviousCannonPress += SwitchToPreviousCannon;
        }

        private void OnDisable()
        {
            gameInput.OnNextCannonPress -= SwitchToNextCannon;
            gameInput.OnPreviousCannonPress -= SwitchToPreviousCannon;
        }


        private void SwitchToNextCannon()
        {
            _canonIndex = (_canonIndex + 1) % Cannons.Length;
            OnCannonSwitch?.Invoke(CurrentCannon);
        }

        private void SwitchToPreviousCannon()
        {
            _canonIndex = _canonIndex - 1 < 0 ? Cannons.Length - 1 : _canonIndex - 1;
            OnCannonSwitch?.Invoke(CurrentCannon);
        }
    }
}
