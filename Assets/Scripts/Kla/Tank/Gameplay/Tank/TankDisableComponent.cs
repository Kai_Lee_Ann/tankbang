﻿using UnityEngine;

namespace Kla.Tank
{
    public class TankDisableComponent : MonoBehaviour
    {
        [SerializeField] private Health health;
        [SerializeField] private TankMover tankMover;
        [SerializeField] private CannonSwitcher cannonSwitcher;
        [SerializeField] private CannonShooter cannonShooter;

        private void OnEnable() => health.OnDeplete += OnDepleted;

        private void OnDisable() => health.OnDeplete -= OnDepleted;


        private void OnDepleted(Health targetHealth)
        {
            DisableTank();
            targetHealth.enabled = false;
        }

        private void DisableTank()
        {
            tankMover.enabled = false;
            cannonSwitcher.enabled = false;
            cannonShooter.enabled = false;
        }
    }
}
