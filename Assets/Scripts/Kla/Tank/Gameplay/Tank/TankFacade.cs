﻿using UnityEngine;

namespace Kla.Tank
{
    public class TankFacade : MonoBehaviour, IUnit
    {
        [SerializeField] private PlayerSettings playerSettings;
        [SerializeField] private TankMover tankMover;
        [SerializeField] private CannonShooter cannonShooter;
        [SerializeField] private Health health;

        public Vector3 Position => transform.position;
        public float MoveSpeed => tankMover.MoveSpeed;
        public float Damage => cannonShooter.Damage;
        public Health Health => health;


        private void Awake()
        {
            health.Init(playerSettings.HealthSettings);
            tankMover.SetMoveSettings(playerSettings.MovementSettings);
        }
    }
}
