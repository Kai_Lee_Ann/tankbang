﻿using UnityEngine;

namespace Kla.Tank
{
    public class TankView : MonoBehaviour
    {
        [SerializeField] private Transform cannonSpawnPoint;
        [SerializeField] private CannonSwitcher cannonSwitcher;
        [SerializeField] private CannonPool cannonPool;

        public CannonView CannonView { private set; get; }

        private CannonSettings _currentCannon;

        private void OnEnable() => cannonSwitcher.OnCannonSwitch += OnCannonSwitched;

        private void OnDisable() => cannonSwitcher.OnCannonSwitch -= OnCannonSwitched;

        private void OnCannonSwitched(CannonSettings cannonSettings) => UpdateCannon(cannonSettings);

        private void UpdateCannon(CannonSettings cannonSettings)
        {
            if (_currentCannon)
                cannonPool.Reclaim(_currentCannon);

            _currentCannon = cannonSettings;
            CannonView = cannonPool.Get(cannonSettings);
            UpdateTransform(CannonView.transform, transform);
        }

        private void UpdateTransform(Transform cannon, Transform tank)
        {
            cannon.parent = tank;
            cannon.position = cannonSpawnPoint.position;
            cannon.forward = tank.forward;
        }
    }
}
