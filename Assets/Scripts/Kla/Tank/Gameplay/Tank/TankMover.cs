﻿using UnityEngine;

namespace Kla.Tank
{
    public class TankMover : MonoBehaviour
    {
        [SerializeField] private Collider areaCollider;
        [SerializeField] private GameInput gameInput;

        public float MoveSpeed => _movementSettings.MoveSpeed;
        private float RotationSpeed => _movementSettings.RotationSpeed;

        private MovementSettings _movementSettings;

        private void Update()
        {
            MoveForward(gameInput.Vertical);
            Rotate(gameInput.Horizontal);
            CheckAreaBounds();
        }


        public void SetMoveSettings(MovementSettings movementSettings) => _movementSettings = movementSettings;

        private void MoveForward(float input)
        {
            var magnitude = input * MoveSpeed * Time.deltaTime;
            transform.Translate(Vector3.forward * magnitude);
        }

        private void Rotate(float input)
        {
            var magnitude = input * RotationSpeed * Time.deltaTime;
            transform.Rotate(Vector3.up, magnitude);
        }

        private void CheckAreaBounds()
        {
            var position = transform.position;
            if (!areaCollider.bounds.Contains(position))
                transform.position = areaCollider.bounds.ClosestPoint(position);
        }
    }
}
