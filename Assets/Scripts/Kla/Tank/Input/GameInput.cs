﻿using System;
using UnityEngine;

namespace Kla.Tank
{
    public class GameInput : MonoBehaviour
    {
        private const float AxisMaxValue = 1f;
        private const float AxisDefaultValue = 0f;

        public float Horizontal { get; private set; }
        public float Vertical { get; private set; }
        public event Action OnShootPress;
        public event Action OnNextCannonPress;
        public event Action OnPreviousCannonPress;

        private void Update()
        {
            UpdateVerticalAxis();
            UpdateHorizontalAxis();
            CheckShoot();
            CheckNextGunPressed();
            CheckPreviousGunPressed();
        }

        private void CheckNextGunPressed()
        {
            if (Input.GetKeyDown(KeyCode.E))
                OnPreviousCannonPress?.Invoke();
        }

        private void CheckPreviousGunPressed()
        {
            if (Input.GetKeyDown(KeyCode.Q))
                OnNextCannonPress?.Invoke();
        }

        private void CheckShoot()
        {
            if (Input.GetKey(KeyCode.X))
                OnShootPress?.Invoke();
        }


        private void UpdateHorizontalAxis()
        {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                Horizontal = AxisMaxValue;
                return;
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                Horizontal = -AxisMaxValue;
                return;
            }

            Horizontal = AxisDefaultValue;
        }

        private void UpdateVerticalAxis()
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                Vertical = AxisMaxValue;
                return;
            }

            if (Input.GetKey(KeyCode.DownArrow))
            {
                Vertical = -AxisMaxValue;
                return;
            }

            Vertical = AxisDefaultValue;
        }
    }
}
