# Tank!
Тестовое задание для [Banzai Games](https://banzai.games/)

[Постановка задачи здесь.](https://docs.google.com/document/d/1yWIRRGxtG-OXTr5zsnPVCFVaoZewpERXGDOAROFw9xI/edit)

## Что можно улучшить
+ Внедрить DI для связывания сущностей
+ Добавить систему для загрузки уровней и по максимуму убрать все элементы со сцены. По факту, на сцене должен остаться только один EntryPoint, который бы отвечал за дальнейшую загрузку всех элементов
+ Внедрить Addressables для доступа к префабам и конфигам
+ Добавить препятствия и движение противников по Navmesh
+ Более серьезный GameInput, вместо заглушки, абстрагировать за интерфейсом
+ Ограничить движение камеры по размеру арены
+ Добавить валидацию конфигов (через кастомный валидатор или заглуши в виде NullObject'ов)

## Что можно улучшить (менее критично)
+ Подсчет очков - условный, никоим образом не должен храниться в UI
+ Random вынести в общий класс (т.к. может понадобится свой алгоритм, хранимое состояние и т.д.)
+ Изменить способ хранения конфигов. Убрать репозитории ScriptableObject и заменить их на загрузку из текстовых или бинарных файлов, которые бы собирались на основе таблиц ГД
+ Вынести в отдельный класс состояние игрока и противников (здоровье, положение, тип). Чтобы можно было сохранять это состояние и загружаться из него
+ Добавить простую анимацию на время неуязвимости
+ Добавить вывод перезарядки оружия
+ Добавить шанс появления противника определенного вида


## Комментарии к решению
+ Общий namespace - сознательный выбор. Плодить множество namespace в данном контексте кажется избыточным и привело бы только к лишним правкам директив `using`. Разграничивать имена сущностей тоже не имело смысла, т.к. они не пересекаются.
+ Принцип использования однострочных выражений - если выражение можно написать без использования `{}` или `=>`, значит так и пишем
+ Порядок форматирования сущностей: Serializable поля, Property, events, поля, методы событий Unity, потом остальные методы. Внутри каждой группы - по строгости доступа (в порядке возрастания, от public до private).
